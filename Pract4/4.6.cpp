#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>

#define LENGTH 40
#define MAXAGE 122 // максимальное значение возраста 
#define MAXN 100 // максимальное количество записей 

int main()
{
    int N = 0;
    printf("Input the number:\n");
    char *young = 0;
    char *old = 0;
    int youngBuf = MAXAGE;
    int oldBuf = 0;

    while(!scanf("%d", &N) || N < 0 && N > MAXN)
    {
        fflush(stdin);
        printf("Insert right number");
    }

    char *family = (char*)malloc(N* sizeof(char)*LENGTH);

    for(int i = 0; i < N; i++)
    {
        int currentage = 0;
        printf("Insert name: \n");
        fflush(stdin);
        fgets((family + LENGTH*i), LENGTH, stdin);
        printf("Insert his age: \n");
        while(!scanf("%d", &currentage) || currentage < 0 && currentage > MAXAGE)
            {
                fflush(stdin);
                printf("Insert right number");
            }
        if(currentage > oldBuf)
        {
            old = (family + LENGTH*i);
            oldBuf = currentage;
        }

        if(currentage < youngBuf)
        {
            young = (family + LENGTH*i);
            youngBuf = currentage;
        }

        fflush(stdin);
    }

    printf("The oldest is: %s His age: %d\n", old, oldBuf);

    printf("The youngest is: %s His age: %d\n", young, youngBuf);
    
    return 0;
}

