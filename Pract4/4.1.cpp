#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 20
#define LENGTH 80

int main()
{
    char MainArr[SIZE][LENGTH];
    char *point [SIZE];
    int counter = 0;

    printf("Input your strings:\n");

    while(counter < SIZE)
    {

        fgets(MainArr[counter], LENGTH, stdin);

            if(MainArr[counter][0] == '\n')
            {
                break;
            }
        
        point[counter] = MainArr[counter];
        counter++;
    }

    for(int i = 0; i < counter; i++)
    {
        int min = i;

        for(int n = i + 1; n < counter; n++)
        {
            if(strlen(point[n]) < strlen(point[min]))
            {
                min = n;
            }
            char *buf = point[i];
            point[i] = point[min];
            point[min] = buf;
        }
        printf("%s", point[i]);
    }

    return 0;
}