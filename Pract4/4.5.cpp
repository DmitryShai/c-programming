#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 20
#define LENGTH 80

int main()
{
    FILE *File = fopen("data.txt", "r+");
    char MainArr[SIZE][LENGTH];
    char *point [SIZE];
    int counter = 0;

    printf("Reading data.txt\n");

    while(counter < SIZE && !feof(File))
    {
        fgets(MainArr[counter], LENGTH, File);

        if(MainArr[counter][strlen(MainArr[counter])-1]=='\n')
            MainArr[counter][strlen(MainArr[counter])-1]=0;

        point[counter] = MainArr[counter];
        counter++;
    }

    fprintf(File,"\n\n");

    for(int i = 0; i < counter; i++)
    {
        fputc('\n',File);
        int min = i;

        for(int n = i + 1; n < counter; n++)
        {
            if(strlen(point[n]) < strlen(point[min]))
            {
                min = n;
            }
        }
        char *buf = point[i];
        point[i] = point[min];
        point[min] = buf;
        fprintf(File,"%s",point[i]);
    }

    fclose(File);
    printf("Check the file\n");
    return 0;
}