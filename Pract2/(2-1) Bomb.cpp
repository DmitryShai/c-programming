// Bomb.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <windows.h>

int main(){
    const float g = 9.81;
    float H, h; // Determined hight (H) and Bomb height (h)
    int check = 0;
    H = 0;
    h = 0;
    int t = 0; // Time
    printf("Insert bombing height in meters \n");
    check = scanf("%f", &H);

    while (check != 1){
        fflush(stdin);
        printf("Insert right height in meters\n");
        check = scanf("%f", &H);
    }
    h = H;

    while (h >= 0){
        h = H - g * t * t/2;
        printf("Curent time %d sec, curent height %.02f meters\n", t, h);
        t++;
        Sleep(1000);
    }

    return 0;
}