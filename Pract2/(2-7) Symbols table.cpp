#include "stdafx.h"
#include <string.h>
#include <stdio.h>

#define LENGTH 80
#define CHARS 256

int main()
{
    char line[LENGTH];
    int mainArr[CHARS] = { 0 };
    printf("Input string:\n");

    fgets(line, LENGTH, stdin);

    if (line[strlen(line) - 1] == '\n'){
        line[strlen(line) - 1] = '\0';
    }

    for (int i = 0; line[i]; i++)
        mainArr[line[i]]++;

    for (int n = 0; n<CHARS; n++)
    {
        if (mainArr[n])
            printf("%c: %d\n", n, mainArr[n]);
    }

    return 0;
}