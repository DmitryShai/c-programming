// stars.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>


int main(){
    int i;
    int stars;
    printf("Insert number of lines\n");
    scanf("%d", &stars);
    while (stars > 40){
        fflush(stdin);
        printf("Insert right number\n");
        scanf("%d", &stars);
    }
    for(i = 0; i<= stars; i++){
        printf("%*s", 40-i, "*");
        for(int j = 0; j < i*2; j++)
            printf("*");
        printf("\n");
    }
    return 0;
}