#include "stdafx.h"
#include <time.h>
#include <stdlib.h>


void play(int magic){
    srand (unsigned (time(0)));
    magic = rand() % 100 + 1;
    int t; // number of retries
    int x;
    x = 0;

    for(t = 0; t < 10; t++){
        printf("Guess magic number: ");
        scanf("%d", &x);

        if(x == magic){
            printf("Thats right! Magic number is %d", x);
            return;
        }else if(x < magic){
            printf("The magic number is bigger\n");
        }else{
            printf("The magic number is smaller\n");
        }
    }
    printf("Too many tries\n Try again.");
}

int main()
{
    int magic;
    int option;
    int check = 0;
    printf(" 1. Play!\n 2. Get a new number\n 3. Exit\n Insert your choice: ");
    check = scanf("%d", &option);

    while (check != 1){
        fflush(stdin);
        printf("Insert right variant\n");
        check = scanf("%d", &option);
    }

    while(option < 1 || option > 3){
        fflush(stdin);
        printf("Insert right variant\n");
        check = scanf("%d", &option);
    }

    switch(option){

        case 1:
            play(magic);
            break;

        case 2:
            magic = rand()%100+1;
            play(magic);
            break;

        case 3:
            printf("Bye!\n");
            return 0;
            break;
    }
}