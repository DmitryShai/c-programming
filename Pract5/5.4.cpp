#include "stdafx.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 20
#define LENGTH 80

FILE *File = fopen("data.txt", "r+");

void printWord(char *pointers){

    while (*pointers && *pointers != ' ' && *pointers != '\n'){
        fputc(*pointers++, File);
    }

}

int getWords(char MainArr[SIZE], char *point[LENGTH]){
    
    int index = 0; // index ��� ����������� ������ ��������� �������
    int Pointindex = 0; // index ��� ���������� ������� ���������� �� �����

    if(MainArr[index] != ' ')
    {
        point[Pointindex++] = MainArr;
    }

    index++;

    while(MainArr[index]){

        if(MainArr[index] != ' ' && MainArr[index - 1] == ' '){
            point[Pointindex++] = MainArr + index;
        }

        index++;
    }

    return Pointindex; // ���������� ���-�� ����
}

int main()
{
    char MainArr[SIZE][LENGTH]; // ������ �� �������
    char *point[LENGTH]; // ������ � ����������� �� �����

    srand(time(NULL));

    int line = 0; // ������� �����
    while(line < SIZE && !feof(File))
    {
        fgets(MainArr[line], LENGTH, File);

        if(MainArr[line][strlen(MainArr[line])-1]=='\n')
            MainArr[line][strlen(MainArr[line])-1]=0;

        line++;
    }

    fputc('\n\n', File);

    for(int i = 0; i < line; i++){
        int counter = getWords(MainArr[i], point); // ������� ���� � ������� ������

        for(int n = 0; n < counter; n++)
        {
            char *currentWord = point[n];
            int newValue = rand()%(counter - n) + n;
            point[n] = point[newValue];
            point[newValue] = currentWord;
            printWord(point[n]);
            fputc(' ', File);
        }
        
        fputc('\n', File);
    }

    fclose(File);
    printf("Check data.txt\n");

    return 0;
}
