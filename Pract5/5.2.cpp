#include "stdafx.h"
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include <windows.h>
#define LENGTH 20
#define HEIGHT 10

void Assembler(char MainArr[HEIGHT][LENGTH])
{
    //top
    for (int h = 0; h < HEIGHT; h++){
        for (int l = 0; l < LENGTH; l++) //left
            printf("%c", MainArr[h][l]);
        for (int l = LENGTH - 1; l >= 0; l--) //right
            printf("%c", MainArr[h][l]);
            putchar('\n');
    }

    //bottom
    for (int h = HEIGHT - 1; h >= 0; h--){
        for (int l = 0; l < LENGTH; l++) //left
            printf("%c", MainArr[h][l]);
        for (int l = LENGTH - 1; l >= 0; l--) //right
            printf("%c", MainArr[h][l]);
        putchar('\n');
    }

}

int main()
{
    char MainArr[HEIGHT][LENGTH];

    while (true)
    {
        system("cls");
        for(int i = 0; i < HEIGHT; i++){
            for(int j = 0; j < LENGTH; j++){
                if(!(rand()%4))
                    MainArr[i][j] = '*';
                else
                    MainArr[i][j] = ' ';
            }
        }

        Assembler(MainArr);
        Sleep(3000);
    }

    return 0;
}