#include "stdafx.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define LENGTH 80

void printWord(char *letter){

    while (*letter && *letter != ' ' && *letter != '\n'){
        putchar(*letter++);
    }

}

int getWords(char MainArr[LENGTH], char *point[LENGTH]){
    
    int index = 0; // index ��� ����������� ������ ��������� �������
    int Pointindex = 0; // index ��� ���������� ������� ���������� �� �����

    if(MainArr[index] != ' ')
    {
        point[Pointindex++] = MainArr;
    }

    index++;

    while(MainArr[index]){

        if(MainArr[index] != ' ' && MainArr[index - 1] == ' '){
            point[Pointindex++] = MainArr + index;
        }

        index++;
    }

    return Pointindex; // ���������� ���-�� ����
}
int main()
{
    char MainArr[LENGTH]; // ������ �� �������
    char *point[LENGTH]; // ������ � ����������� �� �����

    printf("Insert your string: ");
    fgets(MainArr, LENGTH, stdin);
    srand(time(NULL));
    
    if (MainArr[strlen(MainArr) - 1] == '\n')
        MainArr[strlen(MainArr) - 1] = '\0';

    int counter = getWords(MainArr, point); // ������� ����, ��� ���������� �����

    for(int i = 0; i < counter; i++){

        char *currentWord = point[i];
        int newValue = rand()%(counter - i) + i;
        point[i] = point[newValue];
        point[newValue] = currentWord;
        printWord(point[i]);
        putchar(' ');

    }

    putchar('\n');

    return 0;
}

