
#include "stdafx.h"
#include<stdlib.h>
#include<string.h>
#include<time.h>

#define LENGTH 200
#define MAX 80

int Count(char *point[LENGTH], char *MainArr)
{
    int counter = 0;

    if(*MainArr != ' ')
    {
        point[counter++] = MainArr;
    }

    int ilen = strlen(MainArr);
    for(int i = 1; i < ilen; i++){
        if(MainArr[i] != ' ' && (MainArr[i - 1] == ' '))
            point[counter++] = MainArr + i;
    }

    return counter;
}

void wordCrusher(char *CurrentWord)
{
    int size = 0; // ������ �����
    while(*CurrentWord && *CurrentWord != ' ')
    {
        size++;
        CurrentWord++;
    }

    CurrentWord -= size;
    srand(time(NULL));

    for(int i = 1; i < size - 1; i++)
    {
        int random = rand()%(size - 2) + 1;
        char letter = *(CurrentWord + i); // ��������� ������� ����� ��������� �����
        *(CurrentWord + i) = *(CurrentWord + random); // �������� �� ��������� ����� ����� �� �����
        *(CurrentWord + random) = letter;
    }
}

int main()
{
    char MainArr[MAX][LENGTH];
    char *point[LENGTH];
    int lines[MAX]; // ������ � ����������� �� ������
    FILE *File = fopen("data.txt", "at+");
    int word_counter = 0; // ������� ���� � ������
    int line = 0; // ������� �����
    while(line < MAX){
        if(!fgets(MainArr[line], LENGTH, File)){
            if(!line){
                printf("Empty file!\n");
                fclose(File);
                exit(0);
            }
            break;
        }
        line++;
    }

    fputs("\n\n",File);

    for(int n = 0; n < line; n++){
        word_counter = Count(point, MainArr[n]);
        for(int i = 0; i < word_counter; i++)
        {
            wordCrusher(point[i]);
            while (*point[i] && *point[i] != ' '){
                fputc(*point[i]++, File);
            }
            fputc(' ',File);
        }
    }
    fclose(File);
    printf("Check your file\n");
    return 0;
}

