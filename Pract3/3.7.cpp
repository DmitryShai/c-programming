#include "stdafx.h"
#include <string.h>
#include <stdio.h>

#define LENGTH 80
#define CHARS 256

int main()
{
    char line[LENGTH];
    int mainArr[CHARS] = { 0 };
    printf("Input string:\n");
    int max = 0;
    bool CHECK = 1;

    fgets(line, 200, stdin);

    if (line[strlen(line) - 1] == '\n')
        line[strlen(line) - 1] = '\0';

    for (int i = 0; line[i]; i++)
        mainArr[line[i]]++;

    while(CHECK)
    {
        max = 0;
        CHECK = 0;

        for (int n = 0; n<CHARS; n++)
        {
            if (mainArr[n]>mainArr[max])
            {
                max = n;
                CHECK = 1;
            }
        }

        printf("%c: %d\n", max, mainArr[max]);
        mainArr[max] = 0;
    }
                    return 0;
}