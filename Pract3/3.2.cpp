#include "stdafx.h"
#include <stdio.h>

#define LENGTH 80
#define CHARS 256

int main()
{
    char line[LENGTH];
    int letters = 0;
    int index = 0;
    int length = 0;

    printf("Inser a line:\n");
    gets(line);

    while(line[index]){

        if (line[index] != ' '){
            putchar(line[index]);
            length++;
        }

        if (line[index] == ' ' && line[index + 1] != ' ' || line[index + 1] == '\0'){
            printf(": %d\n", length);
            length = 0;
        }
        
        index++;
    }
}