#include "stdafx.h"
#include <stdlib.h>
#include <time.h>

#define N 30
#define RANGE_MAX 100
#define RANGE_MIN -100

int main(){
    int Array[N] = { 0 };
    srand((unsigned)time(NULL));
    int counterN = 0;
    int ind = 0;
    bool LOCK = false;
    int firstSummand = 0;
    int lastSummand = 0;
    int summ = 0;

    printf("Random numbers: \n");

    while(ind < N){
        
        if(counterN > (N / 2) && Array[ind] < 0){
            Array[ind] *= -1;
        }
        
        Array[ind] = rand() % (RANGE_MAX - RANGE_MIN) + RANGE_MIN;

        if(Array[ind] < 0 && !LOCK){
            firstSummand = ind;
            LOCK = true;
        }

        if(Array[ind] > 0){
            lastSummand = ind;
        }

        if(Array[ind] < 0){
            counterN++;
        }

        printf("%3d \n", Array[ind]);
        ind++;
    }

    for (int i = firstSummand; i <= lastSummand; i++){
        summ = summ + Array[i];
    }

    printf("There is %d negative numbers. The sum is: %d\n", counterN, summ);

}