#include "stdafx.h"

#define LENGTH 80

int main()
{
    char line[LENGTH];
    int index = 0;
    int firstSymbol = 0;
    int counter = 0;
    int MaxLength = 0;

    printf("Input your string:\n");
    gets(line);

    while(line[index])
    {
        if(line[index] == line[index + 1]){
            counter++;
        } else { 
            counter = 0; 
        }

        if(counter > MaxLength){
            MaxLength = counter;
            firstSymbol = line[index];
        }

        index++;
    }

    printf("The longest sequence:\n");
    
    for(int i = 0; i < MaxLength; i++){
        putchar(firstSymbol);
    }

    return 0;
}