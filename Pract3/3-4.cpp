#include "stdafx.h"
#include <stdio.h>

#define MAX 4

int transform(char *num, int length){

    int ind = 0;
    int intnum = 0;

    while(length){

        int a = *(num + ind) - '0';

        for(int i = length; i > 1; i--){
            a*=10;
        }

        intnum += a;
        ind++;
        length--;

    }

    return intnum;
}


int main()
{
    char line[200];
    int index = 0;
    int sum = 0;
    int length = 0;
    char number[MAX];
    char *num = NULL;

    printf("Input numbers:\n");

    while (!gets(line)){
        fflush(stdin);
        printf("Something wrong, try again\n");
    }

    while(line[index]){

        if(line[index]>='0' && line[index]<='9' && !(line[index - 1] >= '0' && line[index - 1] <= '9')){
            num = line + index;
        }

        if(line[index]>='0' && line[index]<='9'){
            length++;
        }

        if(length >= MAX){
            sum += transform(num, length);
            num = line + index + 1; // ��������� �� ��������� �����, �.�. num ������ ��������� �� ��������� ����� ����������� �����.
            length = 0;
        }

        index++;

        if(line[index - 1]>='0' && line[index - 1]<='9' && !('0' <= line[index] && line[index] <= '9')){
            sum += transform(num, length);
            length = 0;
        }

    }

    printf("The sum is %d", sum);

}

