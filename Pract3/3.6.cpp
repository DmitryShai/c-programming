// 3.6.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <time.h>

#define N 20
#define RANGE_MAX 100
#define RANGE_MIN 0

int main()
{
    int Array[N] = { 0 };
    int ind = 0;
    srand((unsigned)time(NULL));
    int firstSummand = 0;
    int lastSummand = 0;
    int summ = 0;

    printf("Random numbers: \n");

    while(ind < N){
        
        Array[ind] = rand() % (RANGE_MAX - RANGE_MIN) + RANGE_MIN;
        
        if(Array[firstSummand]>Array[ind]){
            firstSummand=ind;
        }

        if(Array[lastSummand]<Array[ind]){
            lastSummand=ind;
        }
        
        printf("%3d \n", Array[ind]);
        ind++;
    }
    
    for (int i = firstSummand; i <= lastSummand; i++){
        summ = summ + Array[i];
    }
    
    printf("The sum is: %d\n", summ);

	return 0;
}

