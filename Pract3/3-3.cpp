#include "stdafx.h"
#include <stdio.h>

#define LENGTH 80

int main()
{
    char line[LENGTH];
    int index = 0;
    char *BiggestWord = NULL;
    char *CurrentWord = NULL;
    int length = 0;
    int maxlen = 0;

    printf("Insert a line:\n");
    gets(line);

    if (line[index] != ' '){
        BiggestWord = CurrentWord = line;
        length++;
    }

    index++;

    while(line[index]){

        if (line[index] != ' ' && line[index - 1] == ' '){
            CurrentWord = line + index;
        }

        if(line[index] != ' '){
            length++;
        }

        if ((line[index] == ' ' || line[index+1] == '\0') && length > maxlen){
            BiggestWord = CurrentWord;
            maxlen = length;
            length = 0;
        } else if (line[index] == ' '){
            length = 0;
        }

        index++;
    }

    printf("The longest word is ");

    for(int i = 0; i < maxlen; i++){
        putchar(*(BiggestWord + i));
    }

    printf(" It consist of %d words\n", maxlen);

}