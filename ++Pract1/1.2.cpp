#include "stdafx.h"
#include <math.h>
#include <iostream>

#define P 3.14
#define FLOORPRICE 1000
#define BORDERPRICE 2000

class CircleDemo{

public:

    void radius(double _Radius)
    {
        Radius = _Radius;
        if(Radius > 0)
        {
            Area = P*_Radius*_Radius;
            Ference = 2*P*_Radius;
        }
    }

    double Rad()
    {
        return Radius;
    }

    void ference(double _Ference)
    {
        Ference = _Ference;
        if(Ference > 0)
        {
            Radius = _Ference/(2*P);
            Area = P*Radius*Radius;
        }
    }

    double Fer()
    {
        return Ference;
    }

    void area (double _Area)
    {
        Area = _Area;
        if(Area > 0)
        {
            Radius = sqrt(_Area/P);
            Ference = 2*P*Radius;
        }
    }

    double Ar()
    {
        return Area;
    }

    void price(double _Border, double _Runway)
    {
        if(_Border > 0 && _Runway > 0)
        {
            double Fprice = _Runway * FLOORPRICE;
            double Bprice = _Border * BORDERPRICE;
            Price = Fprice + Bprice;
        }
    }

    double Pr()
    {
        return Price;
    }

    void print()
    {
        printf("Pool:\n Radius: %f\n Ference: %f\n Area: %f\n\n", Radius, Ference, Area);
    }

private:
    double Radius;
    double Ference;
    double Area;
    double Price;

};

int main()
{
    CircleDemo c1;  // inner circle 
    CircleDemo c2;  // outer circle 
    c1.radius(3);
    c2.radius(4);
    double border = c2.Fer();
    double runway = c2.Ar() - c1.Ar();
    c2.price(border, runway);
    double summ = c2.Pr();

    c1.print();
    printf("Runway: %f\n", runway);
    printf("Border: %f\n", border);
    printf("Summ: %f\n", summ);
	return 0;
}

