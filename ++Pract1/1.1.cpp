#include "stdafx.h"
#include <math.h>
#include <iostream>

#define P 3.14

class CircleDemo{
public:
    void radius(double _Radius)
    {
        Radius = _Radius;
        if(Radius > 0)
        {
            Area = P*_Radius*_Radius;
            Ference = 2*P*_Radius;
        }
    }

    double Rad()
    {
        return Radius;
    }

    void ference(double _Ference)
    {
        Ference = _Ference;
        if(Ference > 0)
        {
            Radius = _Ference/(2*P);
            Area = P*Radius*Radius;
        }
    }

    double Fer()
    {
        return Ference;
    }

    void area (double _Area)
    {
        Area = _Area;
        if(Area > 0)
        {
            Radius = sqrt(_Area/P);
            Ference = 2*P*Radius;
        }
    }

    double Ar()
    {
        return Area;
    }

    void Fchange(double _Fchange)
    {
        Ference += _Fchange;
        if(Ference > 0)
                {
                    Radius = Ference/(2*P);
                    Area = P*Radius*Radius;
                }
    }

    void print()
    {
        printf("Radius: %f Ference: %f Area: %f\n", Radius, Ference, Area);
    }

private:
    double Radius;
    double Ference;
    double Area;

};

int main()
{
    CircleDemo c1;
    c1.radius(6378.1);
    double Orad = c1.Rad();
    c1.Fchange(0.001);
    double Nrad = c1.Rad() - Orad;
    printf("%f\n", Nrad);
    c1.print();
	return 0;
}

