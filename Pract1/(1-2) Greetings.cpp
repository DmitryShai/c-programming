#include "stdafx.h"
#include <stdio.h> 
#define H 23;
#define MS 59;

void main(void)
{
    int hour = 0;
    int min = 0;
    int sec = 0;
    char gm[][50] = {"Good Morning!\n", "Such a good day!\n", "Good Evening!\n", "It`s too late... Good Night!\n"};
    printf("Insert time in hh:mm:ss\n");
    check = scanf("%02d%02d%02d", &hour, &min, &sec);

    int check = 0;
    while (check != 3){

        fflush(stdin);
        printf("Insert right time\n");
        check = scanf("%02d%02d%02d", &hour, &min, &sec);

    }

    while (hour > H || hour < 0){

        fflush(stdin);
        printf("Insert right hour\n");
        scanf("%02d", &hour);

    }

    while (min > MS || min < 0){

        fflush(stdin);
        printf("Insert right minute\n");
        scanf("%02d", &min);

    }

    while (sec > MS || sec < 0){

        fflush(stdin);
        printf("Insert right second\n");
        scanf("%02d", &sec);

    }

    printf("%02d:%02d:%02d \n", hour, min, sec);

    if (hour > 6 && hour < 10){
    printf("%s", gm[0]);
    }

    if (hour >= 10 && hour < 17){
    printf("%s", gm[1]);
    }

    if (hour >= 17 && hour < 23){
    printf("%s", gm[2]);
    }

    if (hour >= 23 || hour < 6){
    printf("%s", gm[3]);
    }

}