#include <stdio.h>
#include "stdafx.h"
#define WIDTH 80

int main()
{
    char val[WIDTH];
    int len = 0;
    printf("Hello user! Insert a few words\n");
    scanf("%s", &val);
    len = strlen(val);
    printf("%*s\n", (WIDTH-len)/2 + len, val);

    return 0;
}