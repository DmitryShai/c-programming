#include <stdio.h>
#include "stdafx.h"
#define PI 3.14
#define IND 180
int main()
{
    float number = 0;
    char type = 0;
    printf("Hello user! Insert angle value in radians (R) or degrees (D). Form: DD.MMT\n");
    int check = scanf("%f%c", &number, &type);

    while (check != 2){

        fflush(stdin);
        printf("Insert right value or type\n");
        check = scanf("%f%c", &number, &type);

    }

    while (check == 0){

        fflush(stdin);

        if ( type == 'R' || type == 'D' || type == 'r' || type == 'd'){
            check = 1;

        } else {

            check = 0;
            printf("Insert right value or type\n");

        }

    }

    float convert = 0; // convert type
    if (type == 'R' || type == 'r'){

        convert = number * IND / PI;
        printf("%.4f Rad = %.4f Deg\n", number, convert);

    } else if (type == 'D' || type == 'd'){

        convert = number * PI / IND;
        printf("%.4f Deg = %.4f Rad\n", number, convert);

    }

    return 0;
}