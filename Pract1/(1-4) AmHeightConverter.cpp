#include <stdio.h>
#include "stdafx.h"
#define FOOT 0.3048
#define INCH 2.54
int main()
{
    float number = 0;
    float convert = 0;
    char type = 0;
    int check = 0;
    printf("Hello user! Insert height in foots (F) or inches (I).\n");
    check = scanf("%f%c", &number, &type);

    while (check != 2){

        fflush(stdin);
        printf("Insert right value or type\n");
        check = scanf("%f%c", &number, &type);

    }

    while (check == 0){
        fflush(stdin);

        if ( type == 'F' || type == 'I' || type == 'f' || type == 'i'){
            check = 1;

        } else {

            check = 0;
            printf("Insert right value or type\n");

          }

    }

    if (type == 'F' || type == 'f'){

        convert = number * FOOT;
        printf("%.2f foots = %.4f meters\n", number, convert);

    } else if (type == 'I' || type == 'i'){

        convert = number * INCH;
        printf("%.4f inches = %.4f santimeters\n", number, convert);

    }

    return 0;
}